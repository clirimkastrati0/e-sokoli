<p align="center"><a href="https://www.facebook.com/ez.sokoli" target="_blank"><img src="https://scontent.fprn3-1.fna.fbcdn.net/v/t1.6435-9/156835339_102745255214129_2345911147859298116_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=e3f864&_nc_ohc=DQ54plwbeHIAX-WsOKg&_nc_ht=scontent.fprn3-1.fna&oh=00_AT9dJ1IB1-87Z7wRcpnarCZC3dylX_1JqwpD6YVvI47m4g&oe=62D92F47" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About E-Sokoli

Me një përvojë pune mbi 30-vjet, E-Sokoli, ka dëshmuar partneritetin në fushën e ndërtimit duke ofruar cilësi e siguri për çdo ambient ndërtimi që ka marr përsipër. Secilit projekt që është dashur të zbatohet, E-Sokoli gjithmonë i ka ofruar shërbime të veçanta, ndërsa përgjegjësia ndërtuese ka qenë prioritet. 
Duke mbështetur me delikatesë çdo lloj projekti, me cilësi e rehati, E-Sokoli ka fituar shumë projekte të besueshme ndërtimi në tregun e ndërmarrjeve ndërtuese. 
Konstruksionet e objekteve të ndërtimit të lartë, të objekteve të banimit, bizneseve, objekteve shëndetësore e arsimore, studiove të ndryshe dhe thuajse të gjitha llojet e ndërtimeve, E-Sokoli i ka mbështetur gjithmonë duke ofruar shërbime cilësore dhe të një niveli evropian.
Me një staf të kualifikuar dhe me përvojë në fushën e ndërtimtarisë, ne kemi ofruar bashkëpunime konkurruese duke treguar kështu një fokus të veçantë për klientët, të cilët tashmë kanë dëshmuar se jemi shtylla kryesore e një të ardhme moderne. Ne jemi një bashkëpunëtore furnitor, i cili ekzekuton me profesionalizëm shërbimet e nivelit të lartë duke ofruar më të mirën e mundshme për klientin. Këto janë disa nga cilësitë e kompanisë E-Sokoli, të cilat kanë bërë që ne të ekzistojmë për tashmë 30 vjet me radhë e për të cilat gjithmonë gjen zgjidhjen e duhur.

## Web Setup

- Clone this repository on any local web server
- On root folder of the project run: <code>composer install</code>
- Change <code>.env.example</code> to <code>.env</code> and configure app path and database configurations